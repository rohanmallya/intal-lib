
#include<stdio.h>
#include<stdlib.h>
#include<strings.h>
#include<string.h>

char * make_new_string(char* s, int index, char replacement){
	int length = strlen(s);
	char* to_ret = (char*)malloc(sizeof(char)*length+1);
	int i;
	for(i = 0; i < length; i++){
		if(i == index){
			to_ret[i] = replacement;
		}else{
			to_ret[i] = s[i];
		}
	to_ret[length] = '\0';

	}
	return to_ret;
}


int main(){
  char * q = "-893458973420987509874509283742343212111231231231";
	printf("strlen(q) = %d\n",(int)strlen(q));
  char * p = make_new_string(q,0,'+');
  printf("%s,%d\n",p,(int)strlen(p));

	int i;
	int c = 0;
	for(int i = 0; i<strlen(p); i++){
		if(p[i] == '\0'){
			c++;
		}
	}
	printf("C is %d",c);

}
