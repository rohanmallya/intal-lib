//intal.c is a library of integers of arbitrary length.
//intal stands for "int"eger of "a"rbitrary "l"ength.

// intal is an integer of arbitrary length. It has two fields; s and n.
// Field s stores a null-terminated string of decimal digits preceded by
// a + or sign indicating positive and negative integers.
// Field n represents the number of decimal digits excluding the sign.
// n is always greater than zero.
// Eg: Zero is represented with s as "+0" and n == 1.
// Eg: 2017 is represented with s as "+2017" and n == 4.
// Eg: -272 is represented with s as "-272" and n == 3.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "temp1.h"

// Creates and returns a pointer to a new intal initialized to zero.
// Initial values of i would be "+0" and n == 1.
intal* create_intal(){
	intal* i;

	i = (intal*) malloc(sizeof(intal));
	if(i == NULL) {
		return NULL; //malloc failed
	}

	//alloc 3 chars for a null-terminated string "+0".
	i->s = (char *) malloc(3 * sizeof(char));
	if(i->s == NULL) {
		free(i);
		return NULL; //malloc failed
	}

	strcpy(i->s, "+0");
	i->n = 1;

	return i;
}

// Deletes an intal, especially the memory allocated to it.
// i is a pointer to an intal to be deleted. It also resets *i to null.
void delete_intal(intal** i){

	if (i == NULL) {
		return;
	}

	if (*i == NULL) {
		return;
	}

	if( (*i)->s != NULL ) {
		free( (*i)->s );
	}

	free(*i);
	*i = NULL;
	return;
}

// Reads an intal from str into intal struct.
// str should be a null-terminated string just like s inside intal except that
// a postive integer could start without a + sign.
// str is unmodified.
void read_intal(intal* i, char* str){
	int n;

	if(i == NULL) {
		return; //use create_intal before calling read_intal
	}
	if(str == NULL) {
		return; //invalid str
	}

	n = strlen(str);

	if( (str[0] == '+') || (str[0] == '-') ) {
		//it has a sign
	 	i->s = realloc(i->s, n + 1); //one extra char for null termination
	 	strcpy(i->s, str);
		i->n = n;
	} else {
		//it doesn't have a sign and hence it's positive
		n++; //one extra for the + sign
	 	i->s = realloc(i->s, n + 1); //one extra char for null termination
	 	i->s[0] = '+';
	 	strcpy(i->s + 1, str);
		i->n = n;
	}
	return;
}

// Prints the integer into stdout.
// It's just printf of s in intal (with format specifier %s) except that
// it doesn't print the sign in case of positive integer.
void print_intal(intal* i){
	if(i == NULL) {
		return; //no intal to print
	}

	if(i->s != NULL) {
		if(i->s[0] == '+') {
			printf("%s", i->s + 1);
		} else {
			printf("%s", i->s);
		}
	}
	return;
}

char* reverse(char*s){
	int length = strlen(s);
	int i;
	char* to_ret = (char*)malloc(sizeof(char)*length);
	for(i=0;i<length;i++){
		to_ret[i] = s[length-i-1];
	}
	return to_ret;
}

intal* char_to_intal(char * s){
	intal* temp1 = create_intal();
	read_intal(temp1,s);
	return temp1;
}

char* intal_to_char(intal* i){
	return i->s;
}

intal* pad_zeros (intal* a,int length){
	length++;
	char* to_ret = (char*)malloc(sizeof(char)* length);
	int counter = 0;
	int i = 1;
	while(counter< length){
		if(counter == 0){
			to_ret[0] = a->s[0];
		}
		else if(counter < (length-strlen(a->s))){
			to_ret[counter] = '0';
		}else{
			to_ret[counter] = a->s[i];
			i++;
		}

		counter++;
	}
// printf("to_ret in the padded function is %s\n",to_ret);
	return char_to_intal(to_ret);
}
// Adds two intals a and b, and returns the sum.
// Parameters a and b are not modified. Sum is a new intal.
intal* add_intal(intal* a, intal* b){
	int greater;
	intal* local_a = a;
	intal* local_b = b;
	char first_sign_a = local_a->s[0];
	char first_sign_b = local_b->s[0];
	if(first_sign_a == '-' && first_sign_b == '+'){
		// printf("a is %s and length is %d\n\n",local_a->s, strlen(local_a->s) );
		char * temp_local_a = make_new_string(local_a->s,0,'+');
		// printf("temp_local_a is %s and strlen is %d\n\n",temp_local_a,strlen(temp_local_a));
		local_a = char_to_intal(temp_local_a);
		// printf("local_a is %s\n\n",local_a->s);
		return subtract_intal(local_b, local_a);
	}
	else if(first_sign_a == '+' && first_sign_b == '-'){
		char * temp_local_b = make_new_string(local_b->s,0,'+');
		local_b = char_to_intal(temp_local_b);
		return subtract_intal(local_a,local_b);
	}
	else{
		// printf("Entered else with a %s and b%s\n",local_a->s,local_b->s );
		if(local_a->n > local_b->n){
			greater = local_a->n;
			local_b = pad_zeros(local_b,strlen(local_a->s));
		}else{
			greater = local_b->n;
			local_a = pad_zeros(local_a, strlen(local_b->s));
		}

		// printf(" a %s and b%s\n",local_a->s,local_b->s );

		int last_bit_a = (local_a->s[(local_a->n)-1])-'0';
		int last_bit_b = (local_b->s[(local_b->n)-1])-'0';
		char * to_ret = (char*)malloc(sizeof(char)*((greater)+2));
		int n_counter = greater;
		int carry = 0;
		int tr_c = 0;

		// printf("last_bit_b %d, last_bit_a %d",last_bit_b,last_bit_a);
		while(n_counter >= 2){
			int this_ans = last_bit_a + last_bit_b + carry;
			carry = 0;
			// int ans = this_ans + carry;
			// printf("The ans is %d\n",this_ans);

			int ans = 0;
			if(this_ans <= 9){
				ans = this_ans;
				to_ret[tr_c++] = ans+'0';
				// printf("ans0 is %s\n",to_ret);

			}
			if(this_ans > 9){
				char * local_answer = (char*)malloc(sizeof(char)*2);
				sprintf(local_answer,"%d",this_ans);
				// printf("Local Answer is %s\n",local_answer);
				ans = local_answer[1];
				carry = local_answer[0] - '0';
				// printf("Carry is %d\n", carry);
				free(local_answer);
				to_ret[tr_c++] = ans;
				if(n_counter == 2){
					to_ret[tr_c++] = carry + '0';
				}
				// printf("ans1 is %s\n",to_ret);
			}
			// printf("(End while)\n" );
			n_counter--;
			last_bit_a = (local_a->s[(n_counter)-1])-'0';
			last_bit_b = (local_b->s[(n_counter)-1])-'0';

			if(first_sign_a == '-' && first_sign_b == '-'){
				to_ret[tr_c] = '-';
			}
			// printf("%s\n",to_ret);
		}

		return char_to_intal(reverse(to_ret));
	}
	return NULL;
}


char * make_new_string(char* s, int index, char replacement){
	int length = strlen(s);
	char* to_ret = (char*)malloc(sizeof(char)*length+1);
	for(int i = 0; i < length; i++){
		if(i == index){
			to_ret[i] = replacement;
		}
		else{
			to_ret[i] = s[i];
		}
		to_ret[length] = '\0';

	}
	// printf("Made new string %s\n\n",to_ret);
	return to_ret;
}
// Subtracts intal b from intal a. That is, finds a-b and returns the answer.
// Parameters a and b are not modified. a-b is a new intal.
intal* subtract_intal(intal* a, intal* b){
	// Keep in mind the 1000-9 case.
	char sign_a = a->s[0];
	char sign_b = b->s[0];
	// Local Copies
	intal* local_a = a;
	intal* local_b = b;
	int len_a = strlen(a->s);
	int len_b = strlen(b->s);
	// Padding
 	if(len_a > len_b){
		local_b = pad_zeros(b,len_a);
	}else if(len_a < len_b){
		local_a = pad_zeros(a,len_b);
	}
	int len = strlen(local_a->s);
	int temp,swap = 0;
	// They are of equal lengths
	char* result = (char*)malloc(sizeof(char)*(len));
	if((sign_a == '+' && sign_b == '+')){

		if(strcmp(local_a -> s, local_b->s) < 0){
			intal* temp = local_a;
			local_a = local_b;
			local_b = temp;
			swap = 1;

		}
		int i = len-1,k = 0,borrow = 0;
		while(i > 0){
			temp = ((local_a->s[i] - '0') -( local_b->s[i] - '0') - borrow);
			if(temp < 0){
				temp += 10;
				borrow = 1;
			}
			else{
				borrow = 0;
			}
			result[k] = temp + '0';
			k++;
			i--;
		}

		if(swap){
			result[k] = '-';
		}else{
			result[k] = '+';
		}
		return char_to_intal(reverse(result));
	}

	else if(sign_a == '+' && sign_b == '-'){

		char * temp_local_b = make_new_string(local_b->s,0,'+');
		local_b = char_to_intal(temp_local_b);
		return add_intal(local_a,local_b);

	}

	else if(sign_a == '-' && sign_b == '+'){

		char * temp_local_b = make_new_string(local_b->s,0,'-');
		local_b = char_to_intal(temp_local_b);
		// printf("Local_a is %s and b is %s\n",local_a->s, local_b->s );
		return add_intal(local_a,local_b);

	}

	else if(sign_a == '-' && sign_b == '-'){

		char * temp_local_b = make_new_string(local_b->s,0,'+');
		local_b = char_to_intal(temp_local_b);
		return add_intal(local_b,local_a);
	}

	// printf("a is %s and b is %s and ans is (%s)\n",local_a->s,local_b->s,reverse(result));
	// return	char_to_intal(reverse(result));


}
