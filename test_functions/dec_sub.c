intal* subtract_intal(intal* a, intal* b){
	int greater;
	intal* local_a = a;
	intal* local_b = b;
	int b_is_greater;
	if(strcmp(a->s, b->s) > 0){
		b_is_greater = 0;
	}else{
		b_is_greater = 1;
		intal* temp = local_a;
		local_a = local_b;
		local_b = temp;
	}
	char first_sign_a = local_a->s[0];
	char first_sign_b = local_b->s[0];
	if(local_a->n > local_b->n){
		greater = local_a->n;
		local_b = pad_zeros(local_b,strlen(local_a->s));
	}else{
		greater = local_b->n;
		// printf("Else, local_b is %s and local_a is %s\n\n",local_b->s, local_a->s);

		local_a = pad_zeros(local_a,strlen(local_b->s));
	}
	int last_bit_a = (local_a->s[(local_a->n)-1])-'0';
	int last_bit_b = (local_b->s[(local_b->n)-1])-'0';
	// Logic start

	int borrow = 0;
	int n_counter = greater;
	char * to_ret = (char*)malloc(sizeof(char)*((greater)+2));
	to_ret[greater+2] = '\0';

	int tr_c = 0;

	if(first_sign_a == '+' && first_sign_b == '+'){
		// printf("Entered + +, a is %s and b is %s\n",local_a->s, local_b->s);
		while(n_counter >= 2){
			int ans = 0;
			borrow = 0;
			int this_ans = (last_bit_a + borrow) - last_bit_b;
			// printf("ans0 is %d and borrow is %d a is %d and b is %d\n",ans,borrow,last_bit_a,last_bit_b);
			if(this_ans >= 0){
				to_ret[tr_c] = this_ans+'0';
				printf("last_bit a is %d, last_bit_b is %d and borrow is %d\n ans is %d\n",last_bit_a,last_bit_b,borrow,this_ans );

				tr_c++;
			}else{

				int temp = ((local_a->s[n_counter-2]-'0')-1);
				char* temp1 = make_new_string(local_a->s,n_counter-2,temp+'0');
				local_a = char_to_intal(temp1);
				// printf("Local Local_a is %s\n local_b is %s\n\n",local_a->s,local_b->s);
				// local_a->s = make_new_string(local_a->s,n_counter-2,temp+'0');
				// a->s[n_counter-1] = temp + '0';
				borrow = 10;
				ans = (last_bit_a+borrow)-last_bit_b;
				printf("last_bit a is %d, last_bit_b is %d and borrow is %d\n ans is %d\n",last_bit_a,last_bit_b,borrow,ans );
				// printf("ans2 is %d and borrow is %d a is %d and b is %d\n",ans,borrow,last_bit_a,last_bit_b);
				to_ret[tr_c++] = ans+'0';

			}

			n_counter--;
			last_bit_a = (local_a->s[(n_counter)-1])-'0';
			last_bit_b = (local_b->s[(n_counter)-1])-'0';
		}
		if(b_is_greater){
			to_ret[tr_c] = '-';
		}
	}else if(first_sign_a == '+' && first_sign_b == '-'){
		printf("Entered + -\n");
		char * temp_local_b = make_new_string(local_b->s,0,'+');
		local_b = char_to_intal(temp_local_b);
		return add_intal(local_a,local_b);
	}else if(first_sign_a == '-' && first_sign_b == '-'){
		printf("Entered - - \n");
		while(n_counter >= 2){
			int ans = 0;
			borrow = 0;
			int this_ans = (last_bit_b + borrow) - last_bit_a;

			// printf("ans0 is %d and borrow is %d a is %d and b is %d\n",ans,borrow,last_bit_a,last_bit_b);

			if(this_ans >= 0){
				to_ret[tr_c] = this_ans+'0';
				tr_c++;
			}else{

				int temp = ((local_b->s[n_counter-2]-'0')-1);
				local_b->s = make_new_string(local_b->s,n_counter-2,temp+'0');
				// a->s[n_counter-1] = temp + '0';
				borrow = 10;
				ans = (last_bit_b+borrow)-last_bit_a;
				// printf("ans2 is %d and borrow is %d a is %d and b is %d\n",ans,borrow,last_bit_a,last_bit_b);
				to_ret[tr_c++] = ans+'0';


			}
			n_counter--;
			last_bit_a = (local_a->s[(n_counter)-1])-'0';
			last_bit_b = (local_b->s[(n_counter)-1])-'0';
		}

	}else if(first_sign_a == '-' && first_sign_b == '+'){
		printf("Entered = +\n");
		char * temp_local_b = make_new_string(local_b->s,0,'-');
		local_b = char_to_intal(temp_local_b);
		return add_intal(local_a,local_b);
	}
	return char_to_intal(reverse(to_ret));
}
