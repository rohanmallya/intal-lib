#include<stdio.h>
#include<stdlib.h>
#include<strings.h>
#include<string.h>
#include "temp1.h"
#include<math.h>

char* str_pop_one(char* num){
	char* to_ret = (char*)malloc(sizeof(strlen(num)));
	int tr_c = 0;
	for(int i=0;i<strlen(num)-1;i++){
		to_ret[i]  = num[i];
		tr_c++;
	}
	to_ret[tr_c] = '\0';
	return to_ret;

}

char* remove_zero(char* string){
  int len = strlen(string);
  int c = 0;
  for(int i=0;i<len-1;i++){
    if(string[i] == '0' && string[i+1] == '0'){
      c++;
    }
  }
  char* to_ret = (char*)malloc(sizeof(char)*(len-c));
  int k = 0;
  for(int i = c+1; i<len;i++){
    to_ret[k] = string[i];
    printf("k is %c\n",string[i]);
    k++;
  }
  // printf("To_Ret is %s",to_ret);
  return to_ret;
}

intal* multiply_intal(intal* a, intal* b){
	intal* local_a = a;
	intal* local_b = b;

	// We are doing a*b
	int len_a = strlen(local_a->s);
	int len_b = strlen(local_b->s);

  char* string_a = reverse(local_a->s);
  char* string_b = reverse(local_b->s);

  int* to_ret = (int*)malloc(sizeof(int)*(len_a+len_b+1));
  char* to_ret_str = (char*)malloc(sizeof(char)*(len_a+len_b+1));
  for(int i = 0; i< len_b-1; i++){
    int carry = 0;
    for(int j=0; j<len_a-1; j++){
      to_ret[i+j] += carry + ((string_a[j]-'0')*(string_b[i]-'0'));
      carry = to_ret[i+j]/10;
      to_ret[i+j] = to_ret[i+j]%10;
    }
    to_ret[i+len_b] += carry;
  }
  for (int i = 0 ; i < len_a+len_b ; i++)
  {
    to_ret_str[i] = to_ret[i]+'0';
    // printf("This is %d\n",to_ret[i]);
  }

  return char_to_intal(remove_zero(reverse(to_ret_str)));
}

int main(){
	intal* i1 = create_intal();
	intal* i2 = create_intal();

	read_intal(i1, "123");
	read_intal(i2, "66");
  intal* sub_demo = multiply_intal(i1,i2);
	print_intal(sub_demo);
	printf("\n");
  // printf("%s",remove_zero("000123"));



	delete_intal(&i1);

	return 0;

}
