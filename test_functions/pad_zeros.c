#include<stdio.h>
#include<stdlib.h>
#include<strings.h>
#include<string.h>
typedef struct intal {
	char *s; //null terminated string of decimal digits preceded by a +/- sign
	int n; //length of the integer in terms of decimal digits
} intal;

char* pad_zeros(intal* a,int length){
  length++;
	char* to_ret = (char*)malloc(sizeof(char)* length);
	int counter = 0;
	int i = 1;
	while(counter < length){
		if(counter == 0){
			to_ret[0] = a->s[0];
		}
		else if(counter <= (int)(length-strlen(a->s))){
			to_ret[counter] = '0';
		}else{
			to_ret[counter] = a->s[i];
			i++;
		}
		counter++;
}
printf("TO_RET IS %s",to_ret);
return (to_ret);
}
intal* create_intal(){
	intal* i;

	i = (intal*) malloc(sizeof(intal));
	if(i == NULL) {
		return NULL; //malloc failed
	}

	//alloc 3 chars for a null-terminated string "+0".
	i->s = (char *) malloc(3 * sizeof(char));
	if(i->s == NULL) {
		free(i);
		return NULL; //malloc failed
	}

	strcpy(i->s, "+0");
	i->n = 1;

	return i;
}

// Deletes an intal, especially the memory allocated to it.
// i is a pointer to an intal to be deleted. It also resets *i to null.
void delete_intal(intal** i){

	if (i == NULL) {
		return;
	}

	if (*i == NULL) {
		return;
	}

	if( (*i)->s != NULL ) {
		free( (*i)->s );
	}

	free(*i);
	*i = NULL;
	return;
}

// Reads an intal from str into intal struct.
// str should be a null-terminated string just like s inside intal except that
// a postive integer could start without a + sign.
// str is unmodified.
void read_intal(intal* i, char* str){
	int n;

	if(i == NULL) {
		return; //use create_intal before calling read_intal
	}
	if(str == NULL) {
		return; //invalid str
	}

	n = strlen(str);

	if( (str[0] == '+') || (str[0] == '-') ) {
		//it has a sign
	 	i->s = realloc(i->s, n + 1); //one extra char for null termination
	 	strcpy(i->s, str);
		i->n = n;
	} else {
		//it doesn't have a sign and hence it's positive
		n++; //one extra for the + sign
	 	i->s = realloc(i->s, n + 1); //one extra char for null termination
	 	i->s[0] = '+';
	 	strcpy(i->s + 1, str);
		i->n = n;
	}
	return;
}
int main(){
  intal* i1 = create_intal();
  read_intal(i1, "1234");
  pad_zeros(i1,10);

}
