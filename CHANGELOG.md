# Changelog

## v0.0.1


### Added

* Add function
* Subtract function
* Function to convert char* to intal* - `char_to_intal`
* Function to make new string with one replacement - `make_new_string`
* Function to reverse string - `reverse`


### TODO

* Multiplication
* Division
* Power function
* Handle case of addition, subtraction for negative-negative and positive-negative

## v0.0.2

### Changed

* Revamped Add and Subtract Function
* make_new_string function to accomodate end of character

### Added

* Case for subtracting larger integer from smaller integer


## v0.0.3

### Changed

* Logic for subtraction. Previously, it handled case only when borrow was with the neighbor.

### Added

* Test cases for subtraction (++,+-,-+,--). Check `TESTING.md`

## v0.0.4

### Added

* Implemented [Long Multiplication](https://en.wikipedia.org/wiki/Multiplication_algorithm#Long_multiplication)
* Implemented Euclid's Algorithm for divison using repeated subtraction

## v0.1.0

### Added :

* Completly working Multiply function
* Function to Divide
* Function to find power

### Changed :

* `remove_zero` function to handle singular case and return with sign

### Todo:

* Testing of mutliply, divide, power

## v0.1.1

### Added:

* Proper Demo in `intal_demo.c`

### Changed:

* Logic for division, handles all cases
* Test results for multiplication revealed relevancy of answer only until 47 digits.
