//Demo program for the "intal" library of integers of arbitrary length.
//intal stands for "int"eger of "a"rbitrary "l"ength.

#include <stdio.h>
#include "intal.h"
#include <strings.h>
#include <string.h>

int main() {
	intal* i1 = create_intal();
	intal* i2 = create_intal();
	intal* i3 = create_intal();
	intal* i4 = create_intal();
	intal* i5 = create_intal();
	intal* i6 = create_intal();
	intal* i7 = create_intal();
	intal* i8 = create_intal();

	read_intal(i1,"18446744073709551620");
	read_intal(i2,"36893488147419103232");
	read_intal(i7,"1000");
	read_intal(i8,"9");
	read_intal(i3,"110");
	read_intal(i4,"20");
	read_intal(i5,"-110");
	read_intal(i6,"-20");
	//Test Case expected results

	intal * i1_add_i2 = add_intal(i1,i2);
	intal * i1_sub_i2 = subtract_intal(i1,i2);

	intal * i2_sub_i1 = subtract_intal(i2,i1);
	intal * i2_add_i1 = add_intal(i2,i1);

	printf("Demonstrating large number addition and subtraction, number greater than 2**64\n");
	printf("18446744073709551620 + 36893488147419103232 = ");
	print_intal(i1_add_i2);
	printf("\n");
	printf("36893488147419103232 + 18446744073709551620 = ");
	print_intal(i2_add_i1);
	printf("\n");
	printf("36893488147419103232 - 18446744073709551620 = ");
	print_intal(i2_sub_i1);
	printf("\n");
	printf("18446744073709551620 - 36893488147419103232 = ");
	print_intal(i1_sub_i2);
	printf("\n");


	// positive_add_negetive, negetive_add_positive, negetive_add_negetice
	printf("Demonstrating addition of (+,-), (-,+) and (-,-)\n");
	intal* i3_add_i5 = add_intal(i3,i5); // 110 + (-110) = 0
	intal* i5_add_i4 = add_intal(i5,i4); // -110+20 = -90
	intal* i5_add_i6 = add_intal(i5,i6);

	printf("110 + (-110) = ");
	print_intal(i3_add_i5);
	printf("\n");

	printf("-110 + (20) = ");
	print_intal(i5_add_i4);
	printf("\n");

	printf("-110 + (-20) = ");
	print_intal(i5_add_i6);
	printf("\n");


	// negetive_sub_positive, positive_sub_negetive, negetive_sub_negetive
	printf("Demonstrating subtraction of (+,-), (-,+) and (-,-)\n");

	intal* i5_sub_i3 = subtract_intal(i5,i3); // -110 - (110) = -220
	intal* i3_sub_i5 = subtract_intal(i3,i5); // 110 - (-110) = 220
	intal* i5_sub_i6 = subtract_intal(i5,i6); // -110 - (-20) = -110 + 20 = -90

	printf("-110 - (110) = ");
 	print_intal(i5_sub_i3);
 	printf("\n");

	printf("110 - (-110) = ");
	print_intal(i3_sub_i5);
	printf("\n");

	printf("-110 - (-20) = ");
	print_intal(i5_sub_i6);
	printf("\n");

	printf("Demonstrating subtract with multiple borrow\n");

	intal* large_sub_small = subtract_intal(i7,i8);
	printf("1000-9 = ");
	print_intal(large_sub_small);
	printf("\n");


	// Multiplication
	printf("Demonstrating multiplication\n");
	intal* i3_mul_i4 = multiply_intal(i3,i4); //+ * +
	intal* i5_mul_i4 = multiply_intal(i5,i4); //- * +
	intal* i4_mul_i5 = multiply_intal(i4,i5); //+ * -
	intal* i5_mul_i6 = multiply_intal(i5,i6); //- * -

	printf("110 * 20 = ");
	print_intal(i3_mul_i4);
	printf("\n");

	printf("-110 * 20 = ");
	print_intal(i5_mul_i4);
	printf("\n");

	printf("20 * -110 = ");
	print_intal(i4_mul_i5);
	printf("\n");

	printf("-110 * -20 = ");
	print_intal(i5_mul_i6);
	printf("\n");

	// Divison

	printf("Demonstrating division\n");

	intal *i3_div_i4 = divide_intal(i3,i4); // 110/20
	intal *i3_div_i5 = divide_intal(i4,i5);// 110/-110
	intal *i4_div_i6 = divide_intal(i4,i6); // -20/20
	intal *i5_div_i6 = divide_intal(i5,i6); // -110/20

	printf("110/20 = ");
	print_intal(i3_div_i4);
	printf("\n");

	printf("110/-110 = ");
	print_intal(i3_div_i5);
	printf("\n");

	printf("-20/20 = ");
	print_intal(i4_div_i6);
	printf("\n");

	printf("-110/20 = ");
	print_intal(i5_div_i6);
	printf("\n");

	read_intal(i6,"-2");
	
	//Demonstrating Exponential

	intal* i3_exp_i8 = pow_intal(i3,i8); // 110**9 == 2357947691000000000
	intal* i3_exp_i6 = pow_intal(i3,i6); // 110**-20 == null
	printf("110**9 = ");
	print_intal(i3_exp_i8);
	printf("\n");

	printf("110**-2 = ");
	print_intal(i3_exp_i6);
	printf("\n");

	intal* i6_exp_i8 = pow_intal(i6,i8);
	printf("-2**9 = ");
	print_intal(i6_exp_i8);
	printf("\n");

	delete_intal(&i1);
	delete_intal(&i2);
	delete_intal(&i3);
	delete_intal(&i4);
	delete_intal(&i5);
	delete_intal(&i6);
	delete_intal(&i7);
	delete_intal(&i8);



	return 0;
}
