# Functions - What they do, how to use?

* **`void create_intal()`** - used to create an **int**eger of **a**rbitrary **l**ength

* **`void delete_intal(intal* i)`** - used to delete the `intal` i

* **`void read_intal(intal* i,char* num_to_read)`** - reads the char* number into the `intal` i

* **`void print_intal(intal* i)`** - prints the `intal` i

* **`char* reverse(char* s)`** - takes in a string and returns the reverse of it

* **`intal* char_to_intal(char * s)`** - converts a string to `intal` and returns `intal`

*  **`char* intal_to_char(intal* i)`** - converts an `intal` number to a string and returns string

* **`intal* pad_zeros (intal* a,int length)`** - pads 0s to `intal a` to make it of length `length`

* **`char * make_new_string(char* s, int index, char replacement)`** - Replaces with `repalcement` the `index`-th character of string `s`

* **`char* remove_zero(char* string)`** - removes all leading 0s from string `string`

* **`char* add_front(char* string, char thiss)`** - prepends `thiss` to string `string` and returns new string

* **`char* pop_front(char* string)`** - removes the 0th element from `string` and returns new string

* **`intal* add_intal(intal* a, intal* b)`** - adds to intals `a` and `b` and returns sum of type `intal` without modifiying a and b

* **`intal* subtract_intal(intal* a, intal* b)`** - subtracts to intals `a` and `b` and returns difference (a-b) of type `intal` without modifiying a and b

* **`intal* multiply_intal(intal* a, intal* b)`** - multiplies to intals `a` and `b` and returns product (a*b) of type `intal` without modifiying a and b

* **`intal* divide_intal(intal* a, intal* b)`** - divides to intals `a` and `b` and returns quotient (floor(a/b)) of type `intal` without modifiying a and b


* **`intal* pow_intal(intal* a, intal* b)`** - finds a^b and returns new intal with answer
